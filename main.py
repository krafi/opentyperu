import pygame
import random
import sys

# Initialize Pygame
pygame.init()
# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)

# Constants
WIDTH, HEIGHT = 800, 800
FONT_SIZE = 48
LESSON_LEVELS = {
    1: "фыва олдж",
    2: "йцукенгшщз",
    3: "хъэждлорпавы",
    4: "ячсмитьбю",
    5: "эюячыфцув"
}
SEQUENCE_LENGTH = 10

# Themes
THEMES = {
    "Light": {"bg": (255, 255, 255), "text": (0, 0, 0), "highlight": (0, 255, 0)},
    "Dark": {"bg": (0, 0, 0), "text": (255, 255, 255), "highlight": (0, 255, 0)},
    "Peachpuff": {"bg": (255, 218, 185), "text": (0, 0, 0), "highlight": (0, 255, 0)},
    "Dracula": {"bg": (45, 52, 54), "text": (248, 248, 242), "highlight": (139, 233, 253)}
}
DEFAULT_THEME = "Light"

# Screen setup
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Russian Typing Lesson')

# Font setup
font = pygame.font.Font(None, FONT_SIZE)
small_font = pygame.font.Font(None, 36)

def get_random_sequence(letters, length=SEQUENCE_LENGTH):
    return ''.join(random.choice(letters) for _ in range(length))

def draw_text(text, color, pos, font):
    text_surface = font.render(text, True, color)
    screen.blit(text_surface, pos)

def start_screen(selected_theme):
    screen.fill(THEMES[selected_theme]["bg"])
    draw_text("Russian Typing Lesson", THEMES[selected_theme]["text"], (WIDTH//2 - 200, HEIGHT//8), font)
    
    y_offset = HEIGHT//4
    draw_text("Select Level:", THEMES[selected_theme]["text"], (WIDTH//2 - 100, y_offset), small_font)
    y_offset += 50
    for i in range(len(LESSON_LEVELS)):
        draw_text(f"{i + 1}. Level {i + 1}", THEMES[selected_theme]["text"], (WIDTH//2 - 100, y_offset), small_font)
        y_offset += 40

    y_offset += 20
    draw_text("Select Theme:", THEMES[selected_theme]["text"], (WIDTH//2 - 100, y_offset), small_font)
    y_offset += 50
    draw_text("T. Light", THEMES[selected_theme]["text"], (WIDTH//2 - 100, y_offset), small_font)
    y_offset += 40
    draw_text("Y. Dark", THEMES[selected_theme]["text"], (WIDTH//2 - 100, y_offset), small_font)
    y_offset += 40
    draw_text("U. Peachpuff", THEMES[selected_theme]["text"], (WIDTH//2 - 100, y_offset), small_font)
    y_offset += 40
    draw_text("I. Dracula", THEMES[selected_theme]["text"], (WIDTH//2 - 100, y_offset), small_font)

    pygame.display.flip()

def main(level, theme):
    clock = pygame.time.Clock()
    input_text = ""
    current_sequence = get_random_sequence(LESSON_LEVELS[level])

    while True:
        screen.fill(THEMES[theme]["bg"])

        # Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return  # Go back to the start screen
                elif event.key == pygame.K_RETURN:
                    input_text = ""
                    current_sequence = get_random_sequence(LESSON_LEVELS[level])
                elif event.key == pygame.K_BACKSPACE:
                    input_text = input_text[:-1]
                else:
                    input_text += event.unicode

        # Draw the current sequence
        draw_text(f"Type: {current_sequence}", THEMES[theme]["text"], (50, 50), font)

        # Draw the input text
        if input_text == current_sequence[:len(input_text)]:
            color = THEMES[theme]["highlight"]
        else:
            color = RED
        draw_text(input_text, color, (50, 150), font)

        # Draw instructions
        draw_text("Press ESC to return to the main menu", THEMES[theme]["text"], (50, HEIGHT - 50), small_font)

        # Refresh the screen
        pygame.display.flip()
        clock.tick(30)

if __name__ == "__main__":
    selected_theme = DEFAULT_THEME

    while True:
        start_screen(selected_theme)

        # Wait for user to select a level or change the theme
        level_selected = False
        while not level_selected:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key in [pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5]:
                        level_selected = int(event.unicode)
                    elif event.key == pygame.K_t:
                        selected_theme = "Light"
                        start_screen(selected_theme)
                    elif event.key == pygame.K_y:
                        selected_theme = "Dark"
                        start_screen(selected_theme)
                    elif event.key == pygame.K_u:
                        selected_theme = "Peachpuff"
                        start_screen(selected_theme)
                    elif event.key == pygame.K_i:
                        selected_theme = "Dracula"
                        start_screen(selected_theme)

        # Start the selected level
        main(level_selected, selected_theme)
